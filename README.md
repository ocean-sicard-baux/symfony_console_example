
# Symfony console examples

## Documentation

- [Prerequisites](documentation/Prerequisites.md)
- [QA tools](documentation/QA_tools.md)

## License

[AGPL v3](LICENSE)
